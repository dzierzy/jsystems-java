package pl.jsystems.person;

import com.google.common.collect.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonStart {

    public static void main(String[] args) { // psvm
        System.out.println("PersonStart.main"); // sout

        // JCF = Java Collection Framework
        Person kowalski = new Person("Jan", "Kowalski");
        Person iksinski = new Person("Zbigniew", "Iksinski");
        Person nowak = new Person("Adam", "Nowak");

        // SET
        boolean asc = true;
        Comparator<Person> personComparator = //new PersonComparator();
                (o1, o2) -> o1.getLastName().compareTo(o2.getLastName());

        Set<Person> attendees = new TreeSet<>(personComparator);
        attendees.add(kowalski);
        attendees.add(iksinski);
        attendees.add(nowak);
        attendees.add(new Person("Krzysztof", "Kowalski"));

        System.out.println("attendees: " + attendees);

        Person kowalski2 = new Person("Jan", "Kowalski");
        System.out.println("is kowalski attending? "  + attendees.contains(kowalski2));
        attendees.remove(kowalski2);


        int hash1 = kowalski.hashCode();
        int hash2 = kowalski2.hashCode();
        boolean same = false;
        if(hash1==hash2){
            // prawdopodobnie sa takie same
            if(kowalski.equals(kowalski2)){
                same = true;
            }
        } else {
            // na pewno sa rozne
        }

        System.out.println("same: " + same);

        // LIST
        List<Person> personList = Lists.newArrayList(kowalski, nowak, iksinski, kowalski);

        //List<String> names =
        long count =
        //boolean match =
                personList.stream()
                        .filter( p -> !p.getLastName().substring(0,1).equals("N") )

                        .map( p -> p.getFirstName() )
                        .sorted((n1,n2)->n2.compareTo(n1))
                        .distinct()
                        //.limit(2)
                        .peek(n-> System.out.println("from peek: " + n))
                        .count();
                        //.allMatch( n -> n.length() < 4 );
                        //.count();
                        //.collect(Collectors.toList());

        //System.out.println("match: " + match);
        //System.out.println("names: " + names);
        System.out.println("stream items count: " + count);

        // MAP
        Map<Person, Integer> vacations = new TreeMap<>(new PersonComparator(true));
        vacations.put(kowalski, 5);
        vacations.put(nowak, 25);
        vacations.put(iksinski, 12);

        System.out.println("vacation map: " + vacations);
        System.out.println("nowak days left: "  + vacations.get(new Person("Adam", "Nowak")));

        for(Map.Entry<Person, Integer> entry : vacations.entrySet()){
            System.out.println("entry: key=" + entry.getKey() + " value=" + entry.getValue());
        }

        BiMap<Person, Integer> biMap = HashBiMap.create(vacations);
        Person p = biMap.inverse().get(5);
        System.out.println(p);

        Supplier<String> getFirstNameMethodReference = p::getFirstName;

        //Map<Integer, Map<String, Person>>
        Table<Integer, String, Person> table = HashBasedTable.create();


        // STRING LITERALS
        String jsystems = "jsystems"; // new String("jsystems");
        String jsystems2 = "jsystems";

        System.out.println(jsystems == jsystems2);
        System.out.println(jsystems.equals(jsystems2));

        // INTEGER LITERALS
        Integer i1 = new Integer(1); // new Integer(1);
        Integer i2 = 2;

        Set<Integer> ints = new TreeSet<>();
        ints.add(-1);
        ints.add(17);
        ints.add(12);
        ints.add(10);
        ints.add(5);

        System.out.println("ints: ");
        for(int i : ints){
            System.out.print( i + ", ");
        }
        System.out.println();

        System.out.println(i1==i2);
        System.out.println(i1.equals(i2));


    }

}
