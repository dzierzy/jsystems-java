package pl.jsystems.person;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {


    private boolean ascending = false;

    public PersonComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(Person o1, Person o2) {
        //if(ascending)
        int diff = o1.getLastName().length() - o2.getLastName().length();
        if(diff==0){
            return 1;
        } else {
            return diff;
        }
    }
}
