package pl.jsystems.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TrainingHandler extends DefaultHandler {


    boolean nameFlag = false;

    boolean firstNameFlag = false;

    boolean students = false;

    String name;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("name")){
            nameFlag = true;
        } else if(qName.equals("firstName")){
            firstNameFlag = true;
        } else if(qName.equals("students")){
            students = true;
        } else if(qName.equals("fake")){
            System.out.println("fake start");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equals("name")){
            nameFlag = false;
        } else if(qName.equals("firstName")){
            firstNameFlag = false;
        } else if(qName.equals("students")){
            students = false;
        } else if(qName.equals("fake")){
            System.out.println("fake stop");
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if(nameFlag) {
            name = new String(ch, start, length);
        } else if(firstNameFlag && students){
            System.out.println(new String(ch, start, length ));
        }
    }

    public String getName() {
        return name;
    }
}
