package pl.jsystems.xml.sax;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SAXStart {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        System.out.println("SAXStart.main");

        String studentsXml = "./src/main/resources/training.xml";


        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();

        TrainingHandler handler = new TrainingHandler();
        sp.parse(studentsXml, handler);

        System.out.println("training name: " + handler.getName());


    }
}
