package pl.jsystems.xml.xslt;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.util.stream.Stream;

public class XSLTStart {

    public static void main(String[] args) throws TransformerException {
        System.out.println("XSLTStart.main");


        TransformerFactory tf = TransformerFactory.newInstance();

        StreamSource xslt = new StreamSource("./src/main/resources/training.xslt");
        StreamSource xml = new StreamSource("./src/main/resources/training.xml");
        StreamResult html = new StreamResult("./target/training.html");

        Transformer t = tf.newTransformer(xslt);

        t.transform(xml, html);

        System.out.println("done.");


    }
}
