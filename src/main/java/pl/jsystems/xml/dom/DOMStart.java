package pl.jsystems.xml.dom;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DOMStart {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, ParseException, XPathExpressionException {
        System.out.println("DOMStart.main");
        // DOM = Document Object Model

        String studentsXml = "./src/main/resources/training.xml";

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse(studentsXml);

        Element root = doc.getDocumentElement();

        System.out.println("root tag name: " + root.getTagName());
        NamedNodeMap attributes = root.getAttributes();
        for(int i=0; i<attributes.getLength(); i++){
            Node attribute = attributes.item(i);
            System.out.println(attribute);
        }

        String date = attributes.getNamedItem("date").getNodeValue();

        Date trainingDate = new SimpleDateFormat("dd-MM-yyyy").parse(date);
        System.out.println("training date: " + trainingDate);

        NodeList trainingNameList = doc.getElementsByTagName("name");
        if(trainingNameList.getLength()>0){
            System.out.println("node type: " + trainingNameList.item(0).getNodeType());
            if(trainingNameList.item(0).getNodeType()==Node.ELEMENT_NODE){
                String trainingName = trainingNameList.item(0).getTextContent();
                System.out.println("training name: " + trainingName);
            }
        }

        NodeList studentList = doc.getElementsByTagName("student");
        for(int i=0; i<studentList.getLength();i++){
            Element studentNode = (Element)studentList.item(i);
            String id = studentNode.getAttribute("id");
            String firstName = studentNode.getElementsByTagName("firstName").item(0).getTextContent();
            String lastName = studentNode.getElementsByTagName("firstName").item(0).getTextContent();
            System.out.println("student[id=" + id + " firstName=" + firstName + " lastName=" + lastName +"]");
        }


        // XPATH

        System.out.println("XPATH");
        XPathFactory xf = XPathFactory.newInstance();
        XPath xpath = xf.newXPath();

        XPathExpression expression = xpath.compile("//student/firstName");

        NodeList nodeList = (NodeList)expression.evaluate(doc, XPathConstants.NODESET);

        for(int i=0; i<nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            if(node.getNodeType()==Node.ELEMENT_NODE){
                Element element = (Element) node;
                System.out.println(element.getTagName() + " " + element.getAttributes() + " " + element.getTextContent());
            } else {
                System.out.println(node);
            }
        }




        System.out.println("done.");


    }
}
