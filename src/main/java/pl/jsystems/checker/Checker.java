package pl.jsystems.checker;

@FunctionalInterface
public interface Checker<T> {

    boolean check(T t);

    static boolean staticCheck(Integer i){
        return i % 2 == 0;
    }

}
