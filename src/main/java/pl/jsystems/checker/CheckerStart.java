package pl.jsystems.checker;

import java.util.Comparator;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class CheckerStart {

    public static void main(String[] args) {
        System.out.println("CheckerStart.main");

        //CheckerStart cs = new CheckerStart();

        Predicate<Integer> integerChecker = //i -> cs.verify(i);
                                                Checker::staticCheck;

                //i -> i % 2 == 0;

                //i -> i % 2 == 0;
                //new PositiveIntegerChecker();

        System.out.println( integerChecker.test(4) );


        Stream<Integer> stream = Stream.of(2,2,2,2,1,1,1,-4,3,9,-3,-2,7);

        stream
                .filter(integerChecker)
                //.sorted((i1,i2) -> Integer.compare(i1,i2))
                .sorted(Integer::compare)
                .distinct()
                .map(i -> i>=0)
                .forEach(b-> System.out.println(b));




    }


    public static boolean verify(Integer i){
        System.out.println("invoking verify(" + i + ")");
        return i >= 0;
    }

}
