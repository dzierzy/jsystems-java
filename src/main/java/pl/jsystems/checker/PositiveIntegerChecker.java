package pl.jsystems.checker;

public class PositiveIntegerChecker implements Checker<Integer> {
    @Override
    public boolean check(Integer integer) {
        return integer > 0;
    }
}
