package pl.jsystems.fruits;

import pl.jsystems.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FruitStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("let's fruit!");


        Apple a = new Apple();
        List<String> list = a.show(new String("12"));

        Box<Fruit> fruitBox = new Box<>();

        Consumer<Fruit> boxPutMethodReference = fruitBox::put;

        boxPutMethodReference.accept(a);
        //fruitBox.put(a);

        Fruit fruit = fruitBox.get();

        List<Fruit> fruitList = new ArrayList<>();
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));
        fruitList.add(new Apple());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Pear());
        fruitList.add(new Apple("szara reneta", 68));
        fruitList.add(new Apple("antonowka", 86));
        fruitList.add(new Pear("basic", 55));

        /*for(int i =0; i < 1000000; i++){
            fruitList.add(new Apple());
        }
*/

        Stream<Fruit> stream = fruitList.stream();



        List<Fruit> fruits =
        stream
                .filter(f -> f.getCalories() > 60)
                .filter(f -> !f.getName().substring(0,1).equals("s"))
                .sorted( (f1,f2) -> f2.calories - f1.calories)
                //.peek(f->f.setCalories(f.getCalories()*2))
                .distinct()
                .collect(Collectors.toList());

        System.out.println("fruits: " + fruits);



        //fruits =
        Optional<Fruit> optionalFruit =
                fruitList.stream().sorted((fr1, fr2) -> fr1.getCalories()-fr2.getCalories())
                .skip(1)
                .sorted((fr1, fr2) -> fr2.getCalories()-fr1.getCalories())
                .skip(1)
                .findFirst();

                //        .collect(Collectors.toList());
        optionalFruit.ifPresent( f -> System.out.println("from optional: " + f) );


        long startMillis = new Date().getTime();
        long count = fruitList.stream()
                .peek(f -> f.getName())
                //.filter( a -> a.getCalories() <= 50)
                .peek(f -> f.getName())
                .count();
        long endMillis = new Date().getTime();

        System.out.println("count: " + count + " interval: " + (endMillis - startMillis));




    }

}
