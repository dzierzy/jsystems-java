package pl.jsystems.fruits;

public class Box<T extends Fruit> {

    private T element;

    public void put(T o){
        System.out.println("putting fruit " + o.getName());
        element = o;
    }

    public T get(){

        return element;
    }

}
