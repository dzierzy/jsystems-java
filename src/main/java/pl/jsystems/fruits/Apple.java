package pl.jsystems.fruits;

import java.util.ArrayList;
import java.util.List;

public class Apple extends Fruit {


    public Apple() {
        super("apple", 50);
    }

    public Apple(String name, int calories) {
        super( name, calories);
    }

    public <U> List<U> show(U u){
        System.out.println(u.getClass().getName());
        List<U> list = new ArrayList<>();
        list.add(u);
        return list;
    }

    public void appleSpecificMethod(){}

    @Override
    public int getCalories() {
        return 303;
    }
}
