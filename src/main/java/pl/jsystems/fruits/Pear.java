package pl.jsystems.fruits;

public class Pear extends Fruit {

    public Pear(){
        super("pear", 70);
    }

    public Pear(String name, int calories) {
        super("pear " + name, calories);
    }
}
