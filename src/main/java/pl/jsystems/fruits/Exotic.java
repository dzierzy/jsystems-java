package pl.jsystems.fruits;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME) // !!!
@Target({ElementType.TYPE, ElementType.CONSTRUCTOR, ElementType.ANNOTATION_TYPE}) // !!!
@Inherited
@Documented
public @interface Exotic {
    int multiply() default 1;
}
