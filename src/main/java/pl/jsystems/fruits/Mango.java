package pl.jsystems.fruits;

@Exotic(multiply = 3)
public class Mango extends Fruit {

    private String type;

    public Mango() {
        super("mango", 123);
    }
}
