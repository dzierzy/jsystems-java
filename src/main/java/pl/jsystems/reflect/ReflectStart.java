package pl.jsystems.reflect;

import pl.jsystems.charger.Electricity;
import pl.jsystems.charger.Phone;
import pl.jsystems.checker.Checker;
import pl.jsystems.fruits.Apple;
import pl.jsystems.fruits.Exotic;
import pl.jsystems.fruits.Fruit;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.function.Supplier;

public class ReflectStart {

    public static void main(String[] args) throws IllegalAccessException {
        System.out.println("ReflectStart.main");


        //Object o = Electricity.getInstance();
        String className = args[0];

        Supplier<Class> classSupplier = () -> {
            try {
                return Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        };

        Class clazz = classSupplier.get();

        System.out.println("class name: " + clazz.getSimpleName());
        System.out.println("class package :" + clazz.getPackage());
        System.out.println("interface: " + clazz.isInterface());
        System.out.println("primitive: " + clazz.isPrimitive());

        for(Field f : clazz.getDeclaredFields()){
            System.out.println("field: " + f.getType().getName() + " " + f.getName() + " " + Modifier.isPrivate(f.getModifiers()));
            if(Modifier.isStatic(f.getModifiers())){
                boolean wasPrivate = false;
                if(Modifier.isPrivate(f.getModifiers())){
                    f.setAccessible(true);
                    wasPrivate = true;
                }
                System.out.println("field value: " + f.get(null));
                if(wasPrivate){
                    f.setAccessible(false);
                }
            } /*else {
                boolean wasPrivate = false;
                if(Modifier.isPrivate(f.getModifiers())){
                    f.setAccessible(true);
                    wasPrivate = true;
                }
                System.out.println("field value: " + f.get(new Phone(13, "13")));
                if(wasPrivate){
                    f.setAccessible(false);
                }
            }*/
        }

        for(Method m : clazz.getDeclaredMethods()){
            System.out.println("method: "
                    + m.getReturnType().getName() + " " + m.getName() + "(" + Arrays.toString(m.getParameterTypes()) + ")"
            + " throws " + Arrays.toString(m.getExceptionTypes()));
        }

        for(Constructor c : clazz.getConstructors()){
            System.out.println("constructor : " + Arrays.toString(c.getParameterTypes()) + " throws "
                    + Arrays.toString(c.getExceptionTypes()));
        }

        try {
            Constructor c = clazz.getConstructor();
            Object o = c.newInstance();
            //if(o instanceof Fruit){
            if(Fruit.class.isAssignableFrom(o.getClass())){
                Fruit f = (Fruit) o;
                System.out.println(f.getCalories() + " " + f.getName());
                Exotic e = o.getClass().getAnnotation(Exotic.class);
                if(e!=null){
                    // custom code - annotation processing
                    f.setCalories(f.getCalories()*e.multiply());
                }
            } else {
                System.out.println("not a fruit instance");
            }

            for(Annotation a : o.getClass().getAnnotations()){
                System.out.println(a.annotationType());
            }




            /*Method setMethod = clazz.getMethod("setCalories", int.class);
            setMethod.invoke(o, 99);*/

            Method m = clazz.getSuperclass().getMethod("getCalories");

            Object result = m.invoke(o);

            Integer calories = (Integer) result;
            System.out.println("calories from reflection: " + calories);





        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }


    }
}
