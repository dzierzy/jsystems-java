package pl.jsystems.oyster;

public class Oyster {

    private int size;

    private static String description = "description of an oyster";

    public Oyster(int size) {
        this.size = size;
    }

    public void doSomething(){


        class LocalClass{
            private int field;

            public LocalClass(int field) {
                this.field = field;
            }

            @Override
            public String toString() {
                return "LocalClass{" +
                        "field=" + field +
                        ", size=" + size +
                        '}';
            }
        }

        LocalClass localClass = new LocalClass(2);
        localClass.toString();

    }


    public static class Pearl { // non-static -> inner

        private int value;

        public Pearl(int value) {
            this.value = value;
        }

        public String getDescription(){
            return "pearl of value " + value + " produced by oyster of size ?.  oyster description: " + description;
        }

    }

}
