package pl.jsystems.oyster;

public class OysterStart {

    public static void main(String[] args) {
        System.out.println("OysterStart.main");

        Oyster o = new Oyster(5);

        Oyster.Pearl pearl = new Oyster.Pearl(10000);
        System.out.println(pearl.getDescription());
    }

}
