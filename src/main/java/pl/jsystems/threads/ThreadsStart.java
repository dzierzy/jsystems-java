package pl.jsystems.threads;

import pl.jsystems.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("ThreadsStart.main");

        Supplier<Task> taskSupplier = Task::new;
        Runnable task1 = taskSupplier.get();
               /* () -> {
            for (int i = 100; i > 0; i--) {
                System.out.println("i=" + i);
            }
        };*/
        /*Thread t = new Thread(task1); // NEW
        t.start();           // INIT, RUNNING, WAITING/SLEEP, DEAD*/


        Account accountFrom = new Account();
        Account accountTo = new Account();
        //WithdrawTask wt = new WithdrawTask(account, 100_000);
        //DepositTask dt = new DepositTask(new Account(), 100_000);

        Runnable r = () -> {
            for(int i=0; i<100_000; i++) {
                accountFrom.transfer(accountTo, 1);
            }
            System.out.println("from balance: " + accountFrom.getBalance() + ", to balance: " + accountTo.getBalance());
        };

        ExecutorService es = Executors.newFixedThreadPool(1);

        /*Thread t = new Thread(r);
        Thread t2 = new Thread(r);

        t.start();
        t2.start();*/

        es.execute(r);
        es.execute(r);



        System.out.println("done.");

        es.shutdown();

    }

}
