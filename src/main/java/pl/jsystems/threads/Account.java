package pl.jsystems.threads;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger balance = new AtomicInteger(1_000_000);

    public boolean withdraw(int amount){


        //System.out.println("about to withdraw " + amount);
        //synchronized ( this ) {
            if (balance.get()>=amount) {


                balance.addAndGet(amount*-1); // b = b.intValue() - a  1. read current, 2. arithmetic, 3. assign
                return true;
            } else {
                return false;
            }
        //}
    }

    public void deposit(int amount){

        //synchronized ( this ) {
            //System.out.println("about to deposit " + amount + ", current balance " + this.balance );
            balance.addAndGet(amount); // b = b + a, 1. read current, 2. arithmetic, 3. assign
        //}
    }


    public void transfer(Account to, int amount){

        //synchronized (Account.class) {
            this.withdraw(amount);
            to.deposit(amount);
        //}

    }


    public int getBalance() {
        return balance.get();
    }
}
