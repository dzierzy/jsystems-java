package pl.jsystems.cart;

import java.util.ArrayList;
import java.util.List;

public class Cart<T> {

    private List<T> items = new ArrayList<>();

    public List<T> getItems(){
        return items;
    }

    public void addItem(T i){
        items.add(i);
    }
}
