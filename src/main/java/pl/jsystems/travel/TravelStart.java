package pl.jsystems.travel;

import pl.jsystems.person.Person;

import java.time.LocalDate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class TravelStart {

    public static void main(String[] args) {
        System.out.println("TravelStart.main");

        Person person = new Person("Janusz", "Kowalski");

        Supplier<Journey> journeySupplier = getJourneySupplier();

        Journey journey = journeySupplier.get();


        Function<String, Transportation> transportationFunction = (s) -> {
            if(s.equals("air")){
                return new Plane();
            } else {
                return new Horse();
            }
        };

        Transportation t = transportationFunction.apply("air");
                //p -> LocalDate.now().getDayOfWeek().ordinal() > 5;

        Predicate<Person> transportMethodReference = t::transport;

        Consumer<String> stringConsumer = System.out::println;
                //s -> System.out.println(s);
        stringConsumer.accept(t.description());

        journey.travel(person, t);

    }

    public static Supplier<Journey> getJourneySupplier(){
        return Journey::new;
                //() -> new Journey();
    }

}
