package pl.jsystems.travel;

import pl.jsystems.person.Person;

public class Plane implements Transportation {
    @Override
    public boolean transport(Person p) {
        System.out.println("Passenger " + p + " is traveling by plane");
        return true;
    }

    @Override
    public String description() {
        return "LOT";
    }
}
