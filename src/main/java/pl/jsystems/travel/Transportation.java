package pl.jsystems.travel;

import pl.jsystems.person.Person;

@FunctionalInterface
public interface Transportation {

    boolean transport(Person p); // the only abstract method

    default String description(){
        return "unknown";
    }

}
