package pl.jsystems.travel;

import pl.jsystems.person.Person;

public class Journey {


    public Journey() {

        System.out.println("journey constructor");
    }

    public void travel(Person p, Transportation t){
        System.out.println("let's travel!");
        t.transport(p);
    }

}
