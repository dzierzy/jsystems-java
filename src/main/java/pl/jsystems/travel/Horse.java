package pl.jsystems.travel;

import pl.jsystems.person.Person;

public class Horse implements Transportation {
    @Override
    public boolean transport(Person p) {
        System.out.println("Passenger " + p + " traveling by horse");
        return true;
    }

    @Override
    public String description() {
        return "horse";
    }
}
