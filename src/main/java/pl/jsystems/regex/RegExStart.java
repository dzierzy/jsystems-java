package pl.jsystems.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExStart {

    public static void main(String[] args) {
        System.out.println("RegExStart.main");


        //String text = "Lorem orem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Lorem";
        String text = "12-345 ul. Grojecka 1/3, 123-454 Warszawa";

        //String text = "my@mail.pl";
        //String patternText = "\\s[Ll]?orem";
        //String patternText = "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$/";

        String patternText = "[\\s]*\\d\\d-\\d\\d\\d";

        Pattern p = Pattern.compile(patternText);
        Matcher m = p.matcher(text);

        while(m.find()){
            System.out.println(m.start() + " " + m.end());
            System.out.println(m.group());
        }

    }

}
