package pl.jsystems.charger;

import pl.jsystems.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


public class ChargingStart {

    public static void main(String[] args) throws InterruptedException {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));


        Electricity.getInstance().turnOn();

        Charger samsungCharger = new Charger();
        Charger iphoneCharger = new Charger();

        Phone iphone = new Phone(50, "iphone8");
        Phone samsung = new Phone(75, "samsung s8");

        List<Future<Integer>> futures = new ArrayList<>();

        iphoneCharger.chargeDevice(iphone, 2);
        samsungCharger.chargeDevice(samsung, 3);


        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Electricity.getInstance().turnOn();

        System.out.println("done.");

        //Charger.es.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        System.out.println("after await.");

        futures.stream().forEach(f-> {
            try {
                System.out.println("battery level: " + f.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });


        Charger.es.shutdown();


    }


}
