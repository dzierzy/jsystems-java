package pl.jsystems.charger;

public class Phone {

    private int battery = 50;

    private String name;

    private static String description = "just phone";

    public Phone(int battery, String name) {
        this.battery = battery;
        this.name = name;
    }

    public void charge(int circles) {
        for (int i = 1; i <= circles; i++) {

            Electricity e = Electricity.getInstance();
            if(!e.isOn()){
                try {
                    System.out.println("no electricity. waiting...");
                    synchronized (e){
                        e.wait();
                    }
                    System.out.println("electricity is back. continuing...");
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }

            System.out.println("charging phone " + this);
            try {
                Thread.sleep(1 * 1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            battery++;
        }
        System.out.println("charging finished: " + this);
    }

    public int getBattery() throws IllegalArgumentException{
        return battery;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "battery=" + battery +
                ", name='" + name + '\'' +
                '}';
    }
}
