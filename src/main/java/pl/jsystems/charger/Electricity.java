package pl.jsystems.charger;

public class Electricity {

    private static Electricity instance;

    private boolean on = false;


    private Electricity(){}

    public synchronized static Electricity getInstance(){
        if(instance==null){
            instance = new Electricity();
        }
        return instance;
    }

    public void turnOn(){
        on = true;

        Electricity e = Electricity.getInstance();
        synchronized(e){
            e.notifyAll();
        }

    }

    public void turnOff(){
        on = false;
    }

    public boolean isOn() {
        return on;
    }
}
