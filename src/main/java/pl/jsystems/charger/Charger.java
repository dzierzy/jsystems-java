package pl.jsystems.charger;


import java.util.concurrent.*;
import java.util.function.Supplier;

public class Charger{

    private static final int POWER = 3;

    public static ExecutorService es = Executors.newScheduledThreadPool(5);

    public void chargeDevice(Phone p, int hours) {

        Supplier<Integer> supplier = () -> {
            p.charge(hours*POWER);
            return p.getBattery();
        };

        //es.execute(() -> p.charge(hours*POWER));
        //return es.submit(callable);

        try {
            CompletableFuture.
                    supplyAsync(supplier).
                    thenAccept(i-> System.out.println("accepted: " + i)).
                    get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }



}
